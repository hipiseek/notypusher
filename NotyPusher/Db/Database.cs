﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotyPusher.Db
{
    public static class Database
    {
        private static SQLiteAsyncConnection database;
        private static readonly string BackupFolderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
        private static readonly string DatabasePath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "sentences.db3");

        public static async Task Init()
        {
            if (database != null)
                return;

            database = new SQLiteAsyncConnection(DatabasePath);
            await database.CreateTableAsync<Sentence>();
        }

        public static async Task<List<Sentence>> GetSentencesAsync()
        {
            await Init();
            return await database.Table<Sentence>().ToListAsync();
        }
        public static async Task<Sentence> GetSentenceAsync(int id)
        {
            return await database.Table<Sentence>()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public static async Task<int> AddSentenceAsync(Sentence sentence)
        {
            return await database.InsertAsync(sentence);
        }

        public static async Task<int> UpdateSentenceAsync(Sentence sentence)
        {
            return await database.UpdateAsync(sentence);
        }

        public static async Task<int> DeleteSentenceAsync(Sentence sentence)
        {
            return await database.DeleteAsync(sentence);
        }

        public static async Task<int> DeleteAllSentencesAsync()
        {
            return await database.DeleteAllAsync<Sentence>();
        }

        public static async Task BackupDatabase(string name)
        {
            try
            {
                await Init();

                await database.BackupAsync(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), name + ".db3"));
            }
            catch (Exception e)
            {
                Toast.MakeText(null, "Something went wrong", ToastLength.Short).Show();
                return;
            }

            Toast.MakeText(Application.Context, "Backup sucessfully done: " + name, ToastLength.Short).Show();
        }
        public static async Task LoadBackupDatabase(string backupName)
        {
            try
            {
                var backupPath = Path.Combine(BackupFolderPath, backupName + ".db3");
                File.Copy(backupPath, DatabasePath, true);
                database = new SQLiteAsyncConnection(DatabasePath);
                await database.CreateTableAsync<Sentence>();
            }
            catch (Exception e)
            {
                Toast.MakeText(Application.Context, "Something went wrong", ToastLength.Short).Show();
                return;
            }

            Toast.MakeText(Application.Context, "Backup sucessfully loaded: " + backupName, ToastLength.Short).Show();
        }
    }
}