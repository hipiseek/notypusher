﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace NotyPusher.Services
{
    public class NotificationHelper : INotification
    {
        private Context _context;
        private NotificationCompat.Builder _builder;
        public string _notificationChannelId;

        public NotificationHelper()
        {
            _context = Application.Context;
        }
        public void CreateNotification(string title, string message, string id)
        {
                _notificationChannelId = id;
                //try
                //{
                //idk what is it for yet
                //var intent = new Intent(_context, typeof(MainActivity));
                //intent.AddFlags(ActivityFlags.ClearTop);
                //intent.PutExtra(title, message);
                //var pendingIntent = PendingIntent.GetActivity(_context, 0, intent, 0);


                _builder = new NotificationCompat.Builder(_context, _notificationChannelId);
                _builder.SetSmallIcon(Resource.Drawable.abc_ic_star_black_48dp);
                _builder.SetContentTitle(title)
                        .SetAutoCancel(true)
                        .SetContentText(message)
                        .SetPriority((int)NotificationPriority.High)
                        .SetVibrate(new long[0])
                        .SetVisibility((int)NotificationVisibility.Public)
                        .SetSmallIcon(Resource.Drawable.abc_ic_star_black_48dp);
                //.SetContentIntent(pendingIntent);



                NotificationManager notificationManager = _context.GetSystemService(Context.NotificationService) as NotificationManager;

                NotificationImportance importance = NotificationImportance.High;

                NotificationChannel notificationChannel = new NotificationChannel(_notificationChannelId, title, importance);
                notificationChannel.EnableLights(true);
                notificationChannel.EnableVibration(true);
                notificationChannel.SetShowBadge(true);

                if (notificationManager != null)
                {
                    _builder.SetChannelId(_notificationChannelId);
                    notificationManager.CreateNotificationChannel(notificationChannel);
                }

                notificationManager.Notify(0, _builder.Build());


            //}
            //catch (Exception ex)
            //{
            //    //
            //}

        }
    }
}