﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NotyPusher.Services
{
    public interface INotification
    {
        void CreateNotification(string title, string message, string channelId);

    }
}