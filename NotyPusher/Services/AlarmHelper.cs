﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotyPusher.Services
{
    public class AlarmHelper
    {
        private AlarmManager _alarmManager;
        private Context _context;
        private Intent _intent;


        public AlarmHelper(Context context)
        {
            _context = context;
            _alarmManager = (AlarmManager)_context.GetSystemService("alarm");
        }

        public void SetAlarm(string message, int requestCode, int timeInMinutes)
        {
            _intent = new Intent(_context, typeof(AlarmReceiver));
            _intent.PutExtra("title", message.ToString());
            _intent.PutExtra("requestCode", requestCode.ToString());
            _intent.PutExtra("interval", timeInMinutes.ToString());
            PendingIntent pendingIntent = PendingIntent.GetBroadcast(_context, requestCode, _intent, PendingIntentFlags.UpdateCurrent);

            long milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() + timeInMinutes * 1000 * 60;
            _alarmManager.SetExactAndAllowWhileIdle(AlarmType.RtcWakeup, milliseconds, pendingIntent);
        }

        public void CancelAlarm(int requestCode = 1)
        {
            _intent = new Intent(_context, typeof(AlarmReceiver));
            var pendin = PendingIntent.GetBroadcast(_context, requestCode, _intent, PendingIntentFlags.UpdateCurrent);
            _alarmManager.Cancel(pendin);
        }
    }
}