﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotyPusher.Services
{
    public class ConfirmationHelper : PopupWindow
    {
        private Context _context;

        public ConfirmationHelper(Context context)
        {
            _context = context;
        }

        public async Task<bool> Show(string message = "")
        {
            var result = await AlertAsync("Confirmation", message + "\nAre you sure?", "Yes", "No");

            return result;
        }

        public async Task<bool> AlertAsync(string title, string message, string positiveButton, string negativeButton)
        {
            var tcs = new TaskCompletionSource<bool>();

            using (var db = new AlertDialog.Builder(_context))
            {
                db.SetTitle(title);
                db.SetMessage(message);
                db.SetPositiveButton(positiveButton, (sender, args) => { tcs.TrySetResult(true); });
                db.SetNegativeButton(negativeButton, (sender, args) => { tcs.TrySetResult(false); });
                db.Show();
            }

            return await tcs.Task;
        }

    }
}