﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NotyPusher.Services
{
    [BroadcastReceiver]
    public class AlarmReceiver : BroadcastReceiver
    {
        private NotificationHelper _notificationHelper;
        private string _message;
        private AlarmHelper _alarmHelper;

        public override void OnReceive(Context context, Intent intent)
        {
            _alarmHelper = new AlarmHelper(context);
            _message = intent.GetStringExtra("title");
            var requestCode = intent.GetStringExtra("requestCode");
            var timeInMinutes = Convert.ToInt32(intent.GetStringExtra("interval"));
            _notificationHelper = new NotificationHelper();
                        
            _notificationHelper.CreateNotification("GER", _message, requestCode);
            _alarmHelper.SetAlarm( _message,Convert.ToInt32(requestCode), timeInMinutes);

            
        }
    }
}