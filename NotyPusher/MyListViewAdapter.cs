﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NotyPusher.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotyPusher
{
    public class MyListViewAdapter : BaseAdapter<Sentence>
    {
        public List<Sentence> Items { get; set; }
        public Context context;

        public MyListViewAdapter(Context context, List<Sentence> items) : base()
        {
            Items = items;
            this.context = context;
        }


        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }


        public override Sentence this[int position]
        {
            get { return Items[position]; }

        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var view = convertView;
            if (view == null)
            {
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();
                view = inflater.Inflate(Resource.Layout.listview_row, parent, false);

                TextView txtSentence = view.FindViewById<TextView>(Resource.Id.txtSentence);
                txtSentence.Text = Items[position].Text;
                
                TextView txtCategory = view.FindViewById<TextView>(Resource.Id.txtCategory);
                txtCategory.Text = Items[position].Category.ToString();

                TextView timespan = view.FindViewById<TextView>(Resource.Id.txtTimeSpan);
                timespan.Text = Items[position].IntervalInMinutes.ToString() + " minutes";

                if (Items[position].IsActive)
                    view.SetBackgroundColor(Android.Graphics.Color.LightGreen
                        );
            }


            return view;
        }


        //Fill in cound here, currently 0
        public override int Count
        {
            get
            {
                return Items.Count;
            }
        }

    }

    internal class MyListViewAdapterViewHolder : Java.Lang.Object
    {
        public TextView Title { get; set; }
    }


}