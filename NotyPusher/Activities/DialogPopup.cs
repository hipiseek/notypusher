﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NotyPusher.Db;
using NotyPusher.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotyPusher.Activities
{
    [Activity(Label = "DialogPopup")]
    public class DialogPopup : Dialog
    {
        public ConfirmationHelper ConfirmationHelper { get; set; }

        private Sentence _sentence;
        private TextView _dialogText;
        private TextView _interval;
        private CheckBox _checkBox;
        private AlarmHelper _alarmHelper;
        private Button _deleteBtn;
        private Category _category;

        public DialogPopup(Context context, Sentence sentence) : base(context)
        {
            _sentence = sentence;
            _alarmHelper = new AlarmHelper(context);
            ConfirmationHelper = new ConfirmationHelper(context);
        }
        public DialogPopup(Context context) : base(context)
        {
            _alarmHelper = new AlarmHelper(context);
            ConfirmationHelper = new ConfirmationHelper(context);
        }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.popup);
            Window.SetSoftInputMode(SoftInput.AdjustResize);
            // Create your application here

            _dialogText = FindViewById<TextView>(Resource.Id.dialogText);
            _interval = FindViewById<TextView>(Resource.Id.dialogTime);
            _checkBox = FindViewById<CheckBox>(Resource.Id.activateCheckBox);
            _deleteBtn = FindViewById<Button>(Resource.Id.popupDeleteBtn);

            _dialogText.Text = _sentence != null ? _sentence.Text : "";
            _interval.Text = _sentence != null ? _sentence.IntervalInMinutes.ToString() : "";
            _checkBox.Checked = _sentence != null ? _sentence.IsActive : false;
            _deleteBtn.Visibility = _sentence != null ? ViewStates.Visible : ViewStates.Gone;


            var confirmBtn = FindViewById<Button>(Resource.Id.popupSubmitBtn);
            var cancelBtn = FindViewById<Button>(Resource.Id.popupCancelButton);
            _deleteBtn.Click += DeleteBtn_Click;
            confirmBtn.Click += ConfirmBtn_Click;
            cancelBtn.Click += CancelBtn_Click;

            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinner1);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(Spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(Context, Resource.Array.categories_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            if (_sentence != null)
            {
                spinner.SetSelection((int)_sentence.Category);
            }
            else
            {
                spinner.SetSelection(1);
            }


        }
        private void Spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;

            _category = (Category)Enum.Parse(typeof(Category), spinner.GetItemAtPosition(e.Position).ToString());
        }

        private async void ConfirmBtn_Click(object sender, EventArgs e)
        {
            var checkBox = FindViewById<CheckBox>(Resource.Id.activateCheckBox).Checked;
            if (string.IsNullOrWhiteSpace(_interval.Text) || string.IsNullOrEmpty(_dialogText.Text))
            {
                Toast.MakeText(Context, "You cant leave empty fields", ToastLength.Short).Show();
                return;
            }

            if (_sentence is null)
            {
                _sentence = new Sentence
                {
                    IntervalInMinutes = Convert.ToInt32(_interval.Text),
                    IsActive = checkBox,
                    Text = _dialogText.Text,
                    Category = _category
                };

                await Database.AddSentenceAsync(_sentence);

                if (_sentence.IsActive)
                {
                    _alarmHelper.SetAlarm(_sentence.Text, _sentence.Id, _sentence.IntervalInMinutes);
                }

                Dismiss();
            }
            else
            {
                var sentence = await Database.GetSentenceAsync(_sentence.Id);
                sentence.IntervalInMinutes = Convert.ToInt32(_interval.Text);
                sentence.IsActive = checkBox;
                sentence.Text = _dialogText.Text;
                sentence.Category = _category;
                await Database.UpdateSentenceAsync(sentence);

                if (sentence.IsActive)
                {
                    _alarmHelper.SetAlarm(sentence.Text, sentence.Id, sentence.IntervalInMinutes);
                }

                Dismiss();

            }
        }
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Dismiss();
        }
        private async void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (await ConfirmationHelper.Show("You are going to delete this sentence"))
            {
                await Database.DeleteSentenceAsync(_sentence);
            }
            else
            {

            }

            Dismiss();



        }


    }
}