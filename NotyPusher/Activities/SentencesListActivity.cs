﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views;
using System;
using Android.Support.V4.App;
using NotyPusher.Services;
using Android.Content;
using NotyPusher.Db;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using NotyPusher.Activities;
using System.Linq;

namespace NotyPusher
{
    [Activity(Label = "Add new", Theme = "@style/AppTheme")]
    public class SentencesListActivity : AppCompatActivity
    {
        public ConfirmationHelper ConfirmationHelper { get; set; }
        private Context _context;
        private Dialog popupDialog;
        private bool _isArrowUp;

        private ListView _listView;
        private List<Sentence> _list;
        private AlarmHelper _alarmHelper;
        private TextView _textViewInterval;
        private TextView _textViewSentence;
        private TextView _textViewCategory;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            _context = Application.Context;
            _alarmHelper = new AlarmHelper(_context);
            ConfirmationHelper = new ConfirmationHelper(this);

            _listView = FindViewById<ListView>(Resource.Id.listView1);
            var addnewBtn = FindViewById<Button>(Resource.Id.addNewBtn);
            var clearDbBtn = FindViewById<Button>(Resource.Id.addNewDeleteBtn);
            var activateAllBtn = FindViewById<Button>(Resource.Id.activateAllBtn);
            var deactivateAllBtn = FindViewById<Button>(Resource.Id.deactivateAllBtn);
            _textViewSentence = FindViewById<TextView>(Resource.Id.textViewSentence);
            _textViewCategory = FindViewById<TextView>(Resource.Id.textViewCategory);
            _textViewInterval = FindViewById<TextView>(Resource.Id.textViewInterval);

            _textViewInterval.Click += TextViewInterval_Click;
            _textViewSentence.Click += TextViewSentence_Click;
            _textViewCategory.Click += TextViewCategory_Click;

            _listView.ItemClick += ListItem_ItemClick;
            _listView.ItemLongClick += ListItem_ItemLongClick;
            addnewBtn.Click += AddNewBtn_Click;
            clearDbBtn.Click += ClearDbBtn_Click;
            activateAllBtn.Click += ActivateAllBtn_Click;
            deactivateAllBtn.Click += DeactivateAllBtn_Click;


            RefreshDatabase();
        }

        private void TextViewCategory_Click(object sender, EventArgs e)
        {
            if (IsArrowUpShown(_textViewCategory))
            {
                _list = _list.OrderBy(x => x.Category).Reverse().ToList();
            }
            else
            {
                _list = _list.OrderBy(x => x.Category).ToList();
            }

            HideOtherArrows(_textViewSentence, _textViewInterval);

            RefreshList();
        }



        private void TextViewSentence_Click(object sender, EventArgs e)
        {

            if (IsArrowUpShown(_textViewSentence))
            {
                _list = _list.OrderBy(x => x.Text).Reverse().ToList();
            }
            else
            {
                _list = _list.OrderBy(x => x.Text).ToList();
            }

            HideOtherArrows(_textViewCategory, _textViewInterval);

            RefreshList();

        }

        private void TextViewInterval_Click(object sender, EventArgs e)
        {

            if (IsArrowUpShown(_textViewInterval))
            {
                _list = _list.OrderBy(x => x.IntervalInMinutes).Reverse().ToList();
            }
            else
            {
                _list = _list.OrderBy(x => x.IntervalInMinutes).ToList();
            }

            HideOtherArrows(_textViewSentence, _textViewCategory);

            RefreshList();

        }

        private async void ActivateAllBtn_Click(object sender, EventArgs e)
        {
            var sentences = await Database.GetSentencesAsync();

            foreach (var sentence in sentences)
            {
                _alarmHelper.SetAlarm(sentence.Text, sentence.Id, sentence.IntervalInMinutes);
                sentence.IsActive = true;
                await Database.UpdateSentenceAsync(sentence);
            }

            RefreshDatabase();
        }

        private async void DeactivateAllBtn_Click(object sender, EventArgs e)
        {
            var sentences = await Database.GetSentencesAsync();

            foreach (var sentence in sentences)
            {
                _alarmHelper.CancelAlarm(sentence.Id);
                sentence.IsActive = false;
                await Database.UpdateSentenceAsync(sentence);
            }
            RefreshDatabase();
        }

        private async void ListItem_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            var sentenceId = _list[e.Position].Id;

            var sentence = await Database.GetSentenceAsync(sentenceId);
            sentence.IsActive = sentence.IsActive == true ? false : true;

            if (sentence.IsActive)
            {
                _alarmHelper.SetAlarm(sentence.Text, sentence.Id, sentence.IntervalInMinutes);
            }
            else
            {
                _alarmHelper.CancelAlarm(sentence.Id);
            }
            await Database.UpdateSentenceAsync(sentence);
            RefreshDatabase();

        }

        private void ListItem_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var sentenceId = _list[e.Position];
            popupDialog = new DialogPopup(this, sentenceId);
            popupDialog.Show();
            popupDialog.SetCancelable(false);
            popupDialog.DismissEvent += PopupDialog_DismissEvent;
        }

        private async void RefreshDatabase()
        {
            _list = await Database.GetSentencesAsync();

            //old approach
            //List<string> items = new List<string>();
            //foreach (var item in _list)
            //{
            //    items.Add(item.Text);
            //}                        
            //ArrayAdapter adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItemActivated1, items);

            RefreshList();
        }

        private void RefreshList()
        {
            MyListViewAdapter adapter = new MyListViewAdapter(this, _list);

            _listView.Adapter = adapter;
        }

        private void AddNewBtn_Click(object sender, EventArgs e)
        {
            popupDialog = new DialogPopup(this);
            popupDialog.Show();
            popupDialog.SetCancelable(false);
            popupDialog.DismissEvent += PopupDialog_DismissEvent;

            RefreshDatabase();

        }

        private void PopupDialog_DismissEvent(object sender, EventArgs e)
        {
            RefreshDatabase();
        }

        private async void ClearDbBtn_Click(object sender, EventArgs e)
        {
            if (await ConfirmationHelper.Show("You are going to clear database"))
            {
                await Database.DeleteAllSentencesAsync();

            }

        }

        private bool IsArrowUpShown(TextView textView)
        {
            var isArrow = textView.GetCompoundDrawables().Any(x => x != null);

            if (isArrow && _isArrowUp == false)
            {
                textView.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Android.Resource.Drawable.ArrowUpFloat, 0);
                _isArrowUp = true;
                return true;
            }
            else
            {
                textView.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Android.Resource.Drawable.ArrowDownFloat, 0);
                _isArrowUp = false;
                return false;
            }
        }

        private void HideOtherArrows(TextView textView1, TextView textView2)
        {
            textView1.SetCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            textView2.SetCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }
}