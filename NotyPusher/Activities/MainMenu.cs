﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using NotyPusher.Activities;
using NotyPusher.Db;
using NotyPusher.Services;

namespace NotyPusher
{
    [Activity(Label = "@string/app_name", MainLauncher = true, Theme = "@style/AppTheme")]
    public class MainMenu : AppCompatActivity
    {
        public ConfirmationHelper ConfirmationHelper { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.main_menu);
            // Create your application here

            ConfirmationHelper = new ConfirmationHelper(this);

            var btn = FindViewById<Button>(Resource.Id.goToBtn);
            var exitBtn = FindViewById<Button>(Resource.Id.exitBtn);
            var optBtn = FindViewById<Button>(Resource.Id.optionsBtn);
            btn.Click += AddNew;
            exitBtn.Click += ExitBtn_Click;
            optBtn.Click += OptBtn_Click;
        }



        private void OptBtn_Click(object sender, EventArgs e)
        {
            Intent optionsActivity = new Intent(this, typeof(OptionsActivity));
            StartActivity(optionsActivity);
        }


        private void ExitBtn_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void AddNew(object sender, EventArgs e)
        {
            Intent nextActivity = new Intent(this, typeof(SentencesListActivity));
            StartActivity(nextActivity);
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}