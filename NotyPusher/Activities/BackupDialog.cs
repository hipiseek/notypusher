﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NotyPusher.Db;
using NotyPusher.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NotyPusher.Activities
{
    [Activity(Label = "BackupDialog")]
    public class BackupDialog : Dialog
    {
        public ConfirmationHelper ConfirmationHelper { get; set; }

        private ListView _listView;
        private Context _context;
        private TextView _backupName;
        private bool _isLoadingBackup;
        private List<string> _filesNames;
        private string _databaseName;

        public BackupDialog(Context context, bool isLoading) : base(context)
        {
            _context = context;
            _isLoadingBackup = isLoading;
            _filesNames = new List<string>();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.backup_popup);
            Window.SetSoftInputMode(SoftInput.AdjustResize);
            // Create your application here

            ConfirmationHelper = new ConfirmationHelper(_context);

            _listView = FindViewById<ListView>(Resource.Id.backupsListView);
            var submitBtn = FindViewById<Button>(Resource.Id.backupPopupSubmitBtn);
            var cancelBtn = FindViewById<Button>(Resource.Id.backupPopupCancelBtn);
            _backupName = FindViewById<TextView>(Resource.Id.backupDialogText);
            _listView.ItemLongClick += BackupListView_ItemLongClick;

            if (_isLoadingBackup)
            {
                submitBtn.Click += SubmitLoadingBackupBtn;
                _backupName.Visibility = ViewStates.Gone;
                _listView.ChoiceMode = ChoiceMode.Single;
                _listView.ItemClick += ListView_ItemClick; ;
            }
            else
            {
                submitBtn.Click += SubmitMakingBackupBtn;
                _backupName.Visibility = ViewStates.Visible;
                _listView.ChoiceMode = ChoiceMode.None;
            }
            cancelBtn.Click += CancelBtn_Click;

            LoadListOfBackups();
        }

        private async void BackupListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
                var backupName = _filesNames[e.Position];
            if (await ConfirmationHelper.Show("You are going to delete " + backupName + "database."))
            {
                var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), backupName + ".db3");
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                LoadListOfBackups();
            }

        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            _listView.Selected = true;
            _databaseName = _filesNames[e.Position];
        }


        private async void SubmitMakingBackupBtn(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_backupName.Text))
            {
                Toast.MakeText(Context, "You can't leave name empty", ToastLength.Short).Show();
                return;
            }


            if (await ConfirmationHelper.Show("You are going to make backup."))
            {
                await Database.BackupDatabase(_backupName.Text);
                Dismiss();
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Dismiss();
        }

        private async void SubmitLoadingBackupBtn(object sender, EventArgs e)
        {
            if (!_listView.Selected)
            {
                Toast.MakeText(Context, "You have to select database to load", ToastLength.Short).Show();
                return;
            }

            if (await ConfirmationHelper.Show("You are going to load backup. You will lose your current database."))
            {
                await Database.LoadBackupDatabase(_databaseName);
                Dismiss();
            }
        }


        private void LoadListOfBackups()
        {
            var path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
            _filesNames = new List<string>();
            var listOfFiles = Directory.GetFiles(path, "*.db3").ToList();
            foreach (var file in listOfFiles)
            {
                _filesNames.Add(Path.GetFileNameWithoutExtension(file));
            }
            ArrayAdapter adapter = new ArrayAdapter<string>(_context, Android.Resource.Layout.SimpleListItemSingleChoice, _filesNames);

            _listView.Adapter = adapter;
        }
    }
}