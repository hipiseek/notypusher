﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NotyPusher.Db;
using NotyPusher.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotyPusher.Activities
{
    [Activity(Label = "OptionsActivity")]
    public class OptionsActivity : Activity
    {
        public ConfirmationHelper ConfirmationHelper { get; set; }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.options);
            // Create your application here

            ConfirmationHelper = new ConfirmationHelper(this);

            var backupBtn = FindViewById<Button>(Resource.Id.backupBtn);
            var loadBackupBtn = FindViewById<Button>(Resource.Id.loadBackupBtn);

            backupBtn.Click += BackupBtn_Click;
            loadBackupBtn.Click += LoadBackupBtn_Click;
        }

        private void BackupBtn_Click(object sender, EventArgs e)
        {
            var dialog = new BackupDialog(this, false);

            dialog.Show();

        }
        private void LoadBackupBtn_Click(object sender, EventArgs e)
        {
            var dialog = new BackupDialog(this, true);

            dialog.Show();
        }
    }
}